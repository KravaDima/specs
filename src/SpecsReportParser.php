<?php

declare(strict_types=1);

namespace Specs;

use Specs\Contracts\ReportParserContract;
use Specs\Client\Client;
use Specs\Exceptions\SpecsException;

class SpecsReportParser implements ReportParserContract
{
    private const SPECS_SIMPLE = 'en/api/specs_simple';
    private const SPECS = 'en/api/specs';
    private Client $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @throws SpecsException
     */
    public function getSpecsData(string $vincode): string
    {
        $response = $this->client->getClient()->request(
            'get',
            self::SPECS . '?key=' . $this->client->getApiKey() . '&vin=' . $vincode . '&type=specs'
        );

        if ($response->getStatusCode() !== 200) {
            throw new SpecsException('Failed attempt, code ' . $response->getStatusCode() .' ' . __CLASS__);
        }

        $result = $response->getBody()->getContents();
        $resultArray = json_decode($result, true);
        if (empty($resultArray) || $resultArray['status'] === false) {
            throw new SpecsException('Report [specs] not found. Response status [false]');
        }

        return $result;
    }

    /**
     * @throws SpecsException
     */
    public function getSimpleSpecsData(string $vincode): string
    {
        $response = $this->client->getClient()->request(
            'get',
            self::SPECS_SIMPLE . '?key=' . $this->client->getApiKey() . '&vin=' . $vincode . '&type=specs'
        );

        if ($response->getStatusCode() !== 200) {
            throw new SpecsException('Failed attempt, code ' . $response->getStatusCode() .' ' . __CLASS__);
        }

        $result = $response->getBody()->getContents();
        $resultArray = json_decode($result, true);
        if (empty($resultArray) || $resultArray['status'] === false) {
            throw new SpecsException('Report [specs_simple] not found. Response status [false]');
        }

        return $result;
    }
}
