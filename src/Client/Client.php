<?php

declare(strict_types=1);

namespace Specs\Client;

use GuzzleHttp\Client as HttpClient;

class Client
{
    private string $baseUrl;
    private string $apiKey;
    private int $timeout = 90;

    public function __construct(
        string $baseUrl,
        string $apiKey
    ) {
        $this->baseUrl = $baseUrl;
        $this->apiKey = $apiKey;
    }

    public function getClient(): HttpClient
    {
        $headers = [
            'Accept' => 'application/json',
            'User-Agent' => 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36'
        ];

        return new HttpClient([
            'base_uri' => $this->baseUrl,
            'timeout' => $this->timeout,
            'headers' => $headers,
        ]);
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }
}