<?php

return [
    'report_url' => env('SPECS_REPORT_URL'),
    'api_key' => env('SPECS_API_KEY'),
];