<?php

declare(strict_types=1);

namespace Specs\Providers;

use Illuminate\Support\ServiceProvider;
use Specs\Client\Client;
use Specs\Contracts\ReportParserContract;
use Specs\SpecsReportParser;

class SpecsServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', 'specs');

        $this->app->bind(Client::class,
            function () {
                return new Client(config('specs.report_url'), config('specs.api_key'));
            }
        );

        $this->app->bind(ReportParserContract::class, SpecsReportParser::class);
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('specs.php'),
        ], 'config');
    }
}