<?php

declare(strict_types=1);

namespace Specs\Contracts;

interface ReportParserContract
{
    public function getSpecsData(string $vincode): string;
    public function getSimpleSpecsData(string $vincode): string;
}
