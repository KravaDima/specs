<?php

declare(strict_types=1);

namespace Specs\Exceptions;

use Exception;

class SpecsException extends Exception
{
}
